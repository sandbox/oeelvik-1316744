(function ($) {
  
  /**
   * @var string HTML2THEME_CONTAINER_ELEMENTS Comma separated list of selectable container elements
   */
  var HTML2THEME_CONTAINER_ELEMENTS = 'body, div, header, section, aside, footer, nav, article';

  /**
   * @var string HTML2THEME_IFRAME Selector for the editor iframe window
   */
  var HTML2THEME_IFRAME = '#html2theme-editor-window';
  
  /**
   * @var string HTML2THEME_FORM Selector for the editor form
   */
  var HTML2THEME_FORM = '#html2theme-edit-form';
  
  /**
   * @var string HTML2THEME_SELECTION_DISPLAY Selector selector display
   */
  var HTML2THEME_SELECTION_DISPLAY = '#html2theme-selection-display';
  
  /**
   * @var string HTML2THEME_IFRAME_STYLE Styles added to iframe
   */
  var HTML2THEME_IFRAME_STYLE = ''
    + '.html2theme-container-mouseover {'
    + '  outline: 2px solid #838;'
    + '}'
    
    + '.html2theme-container-selected, .html2theme-container-selected.html2theme-container-mouseover {'
    + '  outline: 2px solid #383; !important'
    + '}'
    
    + 'body {'
    + '  margin: 2px; !important' //So that outline is visible for outer elements
    + '}'
    
    + '.region-placeholder .block-region {'
    + '  min-width: 70px;'
    + '  min-height: 70px;'
    + '  background-color: #ffc;'
    + '  border: 1px solid #ff6;'
    + '}';
  
  /**
   * @var boolean containerSelection True if container selection mode is selected.
   * 
   * This will result in a search for a parent container if a non container element is selected
   */
  var containerSelection = true;
  
  /**
   * @var array[stirng] selectedPath xPath to selected element
   */
  var selectedPath = null;
  
  /**
   * @var DOMElement selectedElement Element selected for editing
   */
  var selectedElement = null;
  
  /**
   * @var DOMElement prevSelectedElement Previous element selected for editing
   */
  var prevSelectedElement = null;
  
  /**
   * @var boolean selectedIsContainer True if selected element is container
   */
  var selectedIsContainer = false;
  
  /**
   * @var array history Contains history of all actions
   */
  var history = [];
  
  
  
  /**
   * Start when document.ready
   */
  $(document).ready(function () {
    console.log("document ready"); //Debug
    console.log(Drupal.settings);
    
    setupToolbar();
    
    $(HTML2THEME_FORM).submit(function(){
      $("#html2theme-actions").val(JSON.stringify(history));
      return true;
    });
    
    //Iframe content ready
    $(HTML2THEME_IFRAME).load(function () {
      console.log("iframe ready"); //Debug
      
      //Listen for all clicks inside iframe
      $(HTML2THEME_IFRAME).contents().get()[0].onclick = html2themeEvent;
      
      //Add style to iframe
      $(HTML2THEME_IFRAME).contents().find('head').append('<style type="text/css" media="all">' + HTML2THEME_IFRAME_STYLE + '</style>');
      
      //Outline selected containers
      $(HTML2THEME_IFRAME).contents().find(HTML2THEME_CONTAINER_ELEMENTS).mouseover(function (){
        $(this).addClass('html2theme-container-mouseover');
      }).mouseout(function (){
        $(this).removeClass('html2theme-container-mouseover');
      });
    });
    
  });
    
  /**
   * Handles document.click inside iframe
   * 
   * @param EventObject e
   * 
   * @return boolean false Always false to stop browser from following links 
   */
  function html2themeEvent(e) {
    if (!e) {
      var e = window.event;
    };
    
    //Get clicked element
    if (e.target) var element = e.target;
    else if (e.srcElement) var element = e.srcElement;
    
    //Retrive first Container element and select
    selectElement(element);
    
    return false;
  }
  
  /**
   * Handeles selection of a container element
   * 
   * @param DOMElement element The Container element we would like to select
   */
  function selectElement(element){
    if(containerSelection) element = getContainerElement(element);
    if(!element) return;
    
    
    prevSelectedElement = selectedElement;
    selectedPath = getXPath(element, [], true);
    selectedElement = element;
    selectedIsContainer = isContainer(selectedElement);
    
    //Show selection in selection display and outline selection
    showSelection(element)
    
    updateToolbar();
  }
  
  /**
   * Show selection in selection display and outline selection
   */
  function showSelection(element){
    //Get reference to display element
    var display = $(HTML2THEME_SELECTION_DISPLAY);
    
    //Remove content from selection display
    display.contents().remove();
    
    //Get parent elements and xPath selectors
    var xPath = getXPath(element);
    var parents = $(element).parents();
    
    //Loop through all xPath selectors
    for(var i = 0; i < xPath.length; i++){
      //Select element represented by current xPath
      if(i < xPath.length - 1)
      {
        var el = parents[xPath.length - 2 - i];
      }
      else { //Last xPath selector, not parent but current element
        var el = element;
        var last = true;
      }
      
      //Add seperator between xPath selectors
      if(i > 0){
        var spacer = $(document.createElement('span')).append(' &gt; ');
        display.append(spacer);
      }
      
      //Add anchor to selector display
      var anchor = $(document.createElement('span')).append(xPath[i]);
       
      anchor.click({element: el }, function (e){
        if(!containerSelection || isContainer(e.data.element)) selectElement(e.data.element);
      }).mouseover({element: el }, function (e){
        if(!containerSelection || isContainer(e.data.element))$(e.data.element).mouseover(); //Trigger represented elements mouseover
      }).mouseout({element: el }, function (e){
        if(!containerSelection || isContainer(e.data.element))$(e.data.element).mouseout(); //Trigger represented elements mouseout
      });
      
      //Last element (the selected one)
      if(last) {
        anchor.addClass("selected");
      }
      
      display.append(anchor);
    }

    //Add class to selected element
    $(prevSelectedElement).removeClass('html2theme-container-selected');
    $(element).addClass('html2theme-container-selected');
  }
  
  /**
   * Return element or the first parent element matching the container filter
   * 
   * @param DOMElement element Selected element
   * 
   * @return DOMElement Container element
   */
  function getContainerElement(element){
    if(isContainer(element)) return element; //Selected element is container
    $r = $(element).parents(HTML2THEME_CONTAINER_ELEMENTS).first()[0];
    if(!$r) $r = $(HTML2THEME_IFRAME).contents().find(HTML2THEME_CONTAINER_ELEMENTS).first()[0];
    if(!$r) $r = false;
    return $r; //First parent container
  }
  
  function setupToolbar(){
    $('#html2theme-tool-filter-containers').click(function(){
      containerSelection = $(this).attr("checked"); 
    });

    $('#html2theme-tool-delete').click(del);
    $('#html2theme-tool-clear').click(clear);
    
    $('#html2theme-tool-insert').click(insert);
    $('#html2theme-cansel-insert').click(canselInser);
    buildInsertDialog();
    
    updateToolbar();
  }
  
  function updateToolbar(){
    //Hide dialogs
    $('#html2theme-insert-dialog').css('display', 'none');
    
    //Enable/disable buttons
    $('#html2theme-tool-delete').attr('disabled', !isDeletable());
    $('#html2theme-tool-clear').attr('disabled', !isClearable());
    $('#html2theme-tool-insert').attr('disabled', !isContainer());
  }
  
  function isDeletable(){
    return ($(selectedElement).parents().filter('body').length > 0);
  }
  
  function del() {
    if(!isDeletable()) return;
    
    //Store parent for later selection
    var el = $(selectedElement);
    var parent = el.parent().first()[0];
    
    //delete selected element
    el.remove();
    
    updateHistory({xPath: selectedPath, action: 'delete'});
    
    //Select parent to deleted
    selectElement(parent);
  }
  
  function isClearable(){
    return ($(selectedElement).contents().length > 0);
  }
  
  /**
   * Clear a element for contents
   */
  function clear() {
    if(!isClearable()) return;
    
    //Remove contents
    $(selectedElement).contents().remove();
    
    updateHistory({xPath: selectedPath, action: 'clear'});
    
    //Always reselect after change to trigger update actions
    selectElement(selectedElement);
  }
  
  /**
   * Insert into selectedContainer
   */
  function insert(){
    if(!isContainer()) return;
    
    //Show dialog
    $('#html2theme-insert-dialog').css('display', 'inline-block');
  }
  
  function doInsert(e){
    if(!isContainer() || !e.data.variable && !e.data.region) return;
    
    if(e.data.variable) {
      $(selectedElement).append(e.data.variable.placeholder);
      
      var str = 'insert variable ' + e.data.variable.name + ' into: ' + selectedPath.join(' ');
      updateHistory({xPath: selectedPath, action: 'insert', str: str, args: {type: 'var', name: e.data.variable.name}});
    }
    else if(e.data.region) {
      
      var block = $(document.createElement('div')).addClass('block-region').append(e.data.region.title);
      var region = $(document.createElement('div')).addClass('region-placeholder region region-' + e.data.region.name).append(block);
      $(selectedElement).append(region);

      var str = 'insert region ' + e.data.region.name + ' into: ' + selectedPath.join(' ');
      updateHistory({xPath: selectedPath, action: 'insert', str: str, args: {type: 'region', name: e.data.region.name}});
    }
    
    //Always reselect after change to trigger update actions
    selectElement(selectedElement);
  }
  
  /**
   * Cansel insert action
   *
   * This closes the insert dialog
   */
  function canselInser(){
    //Hide dialog
    $('#html2theme-insert-dialog').css('display', 'none');
  }
  
  /**
   * Populates the insert dialog
   */
  function buildInsertDialog() {
    var dialog = $('#html2theme-insert-dialog .content');
    
    //For each var group
    var groups = Drupal.settings.html2theme.themeableVars;
    for(var i = 0; i < groups.length; i++){
      var group = groups[i];
      var groupElement = $(document.createElement('fieldset')).append($(document.createElement('legend')).append(group.groupname));
      dialog.append(groupElement);
      
      var ul = $(document.createElement('ul'));
      groupElement.append(ul);
      
      //For each insertable variable
      for(var j = 0; j < group.vars.length; j++){
        var v = group.vars[j];
        
        var varelement = $(document.createElement('li'))
          .append($(document.createElement('a'))
            .append(v.title)).
            click({variable: v}, doInsert);
        ul.append(varelement);
        
      }
    }
    
    //Add insert regions for page.tpl.php
    if(Drupal.settings.html2theme.themeFile == 'page.tpl.php'){
      var fieldset = $(document.createElement('fieldset')).append($(document.createElement('legend')).append('Regions'));
      dialog.append(fieldset);
      
      var ul = $(document.createElement('ul'));
      fieldset.append(ul);
      
      //For each default region
      var regions = Drupal.settings.html2theme.defaultRegions;
      for(var j = 0; j < regions.length; j++){
        var region = regions[j];
        
        //Add link to insert region
        var regionElement = $(document.createElement('li'))
          .append($(document.createElement('a'))
            .append(region.title)).
            click({region: region}, doInsert);
        ul.append(regionElement);
        
      }
    }
  }
  
  /**
   * Check if element is a valid container
   * 
   * @param optional DOMElement element Element to check if null, selectedElement is used
   * 
   * @return boolean True if element is container 
   */
  function isContainer(element){
    if(!element) return selectedIsContainer;
    return ($(element).filter(HTML2THEME_CONTAINER_ELEMENTS).get().length > 0);
  }
  
  /**
   * Update action history
   */
  function updateHistory(actionObject){
    var val = history.length;
    history[val] = actionObject;
    
    if(!actionObject.str) actionObject.str = actionObject.action + ': ' + actionObject.xPath.join(' ');
    
    var option = $(document.createElement('option')).attr("value", val).text(actionObject.str);
    $('#html2theme-editor-history-browser').append(option);
  }
  
  /**
   * Get the unique xPath of selected node
   * 
   * @param DOMElement node Node to find path fore
   * @param optional array[string] parh Array of xPath node selectors
   * @param optional boolean breakOnId True to stop traversing if a node with id is added
   * 
   * @return array[string] parh Array of xPath node selectors
   */
  function getXPath(node, path, breakOnId) {
    path = path || [];
    breakOnId = breakOnId || false;
    
    if(!(breakOnId && node.id)){
      if(node.parentNode) {
        path = getXPath(node.parentNode, path, breakOnId);
      }
    }
  
    if(node.previousSibling) {
      var count = 1;
      var sibling = node.previousSibling
      do {
        if(sibling.nodeType == 1 && sibling.nodeName == node.nodeName) {count++;}
        sibling = sibling.previousSibling;
      } while(sibling);
      if(count == 1) {count = null;}
    } else if(node.nextSibling) {
      var sibling = node.nextSibling;
      do {
        if(sibling.nodeType == 1 && sibling.nodeName == node.nodeName) {
          var count = 1;
          sibling = null;
        } else {
          var count = null;
          sibling = sibling.previousSibling;
        }
      } while(sibling);
    }
  
    if(node.nodeType == 1) {
      path.push(node.nodeName.toLowerCase() + (node.id ? "[@id='"+node.id+"']" : count > 0 ? "["+count+"]" : ''));
    }
    return path;
  };
  

})(jQuery);
