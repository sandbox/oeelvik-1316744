<?php

/**
 * Classes that are part of the html2theme module.
 *
 * @file
 */
 
/**
 * Handle files in html2theme module
 */
class Html2themeFileHandeler {
  
  /**
   * @var string $title Name as written in .info file.
   */
  protected $title;
   
   /**
   * @var string $name Name of theme (path friendly).
   */
  protected $name;
  
  /**
   * @var string $dir Directory to theme.
   */
  protected $dir;
  
  /**
   * Get the theme title.
   *
   * @return string Theme title
   */
  public function getTitle(){
    return $this->title;
  }
  
  /**
   * Get the theme name.
   *
   * @return string Theme name
   */
  public function getName(){
    return $this->name;
  }
  
  /**
   * Get the theme directory.
   *
   * @return string Theme directory
   */
  public function getDirectory(){
    return $this->dir;
  }
  
  public function __construct($title) {
    $this->title = $title;
    $this->name = $this->parseName();
    $this->dir = $this->createDirectoryPath();
  }
  
  /**
   * Return an instance of this class.
   *
   * @param string $title
   *   Theme title.
   *
   * @return Html2themeThemeHandeler
   */
  public static function factory($title) {
    return new Html2themeFileHandeler($title);
  }
  
  /**
   * Check if theme title is valid.
   *
   * @return boolean true if valid
   */
  public function titleIsValid(){
    return preg_match('/^[ 0-9A-Za-z]*$/', $this->getTitle());
  }
  
  /**
   * Generate a path friendly version of title to us in file system.
   *
   * @return string name
   */
  public function parseName() {
    return str_replace(" ", "", strtolower($this->getTitle()));
  }
  
  /**
   * Check if theme alrady exists
   * 
   * @return true if theme exists
   */
  public function themeExists(){
    return file_exists($this->getDirectory());
  }
  
  /**
   * Import html with resources from a url.
   */
  public function createFromURL($url){
    if($this->themeExists()) return FALSE;
    
    //Create directories
    drupal_mkdir($this->getDirectory());
    drupal_mkdir($this->getDirectory() . '/static/');
    
    $copier = new Html2themeWebPageCopier($url, $this->getDirectory() . '/static');
    
    return TRUE;
  }
  
  /**
   * Handle uploade of static html page
   * 
   * @param string $field name of uploade field
   */
  public function createFromUploade($field){
    
    if($this->themeExists()) return FALSE;
    
    //Create directories
    drupal_mkdir($this->getDirectory());
    drupal_mkdir($this->getDirectory() . '/static/');
    
    //Save file to temp
    $validators = array('file_validate_extensions' => array('htm html ' . archiver_get_extensions()));
    if (!($finfo = file_save_upload($field, $validators, NULL, FILE_EXISTS_REPLACE))) {
      // Failed to upload the file. file_save_upload() calls form_set_error() on
      // failure.
      return FALSE;
    }
    
    //Check if archive or html
    $path_info = pathinfo($finfo->uri);
    if(strpos(archiver_get_extensions(), $path_info['extension']) !== FALSE) { //Is archive
      //Unpack archive
      try {
        $archive = $this->extractArchive($finfo->uri, $this->getDirectory() . '/static');
      }
      catch (Exception $e) {
        form_set_error($error_field, $e->getMessage());
        return FALSE;
      }
      
      //Check if archive contained any files
      $files = $archive->listContents();
      if (!$files) {
        form_set_error($error_field, t('Provided archive contains no files.'));
        return FALSE;
      }
    }
    else { //is html file
      //Copy and rename file
      copy($finfo->uri, $this->getDirectory() . '/static/index.html');
    }
    
    return TRUE;
  }
  
  public function createInfoFile(){
    $str = 'name = ' . $this->getTitle() . "\n"
      . 'description = html2theme generated theme' . "\n"
      . 'core = ' . DRUPAL_CORE_COMPATIBILITY . "\n"
      . 'engine = phptemplate' . "\n";
    
    //Use QueryPath to select all resources
    $qp = htmlqp($this->getDirectory() . '/static/index.html', '[href], [src]');
    
    //Loop through all
    foreach($qp->get() as $res ){
      if($res->tagName == 'a') continue;
      
      $dir = '/div';
      switch ($res->tagName){
        case 'link':
          if($res->getAttribute('rel') == 'stylesheet'){
            if(!$media = $res->getAttribute('media')) $media = 'all';
            $str .= "\nstylesheets[" . $media . '][] = static/' . $res->getAttribute('href');
          }
          break;
        case 'script':
            $str .= "\nscripts[] = static/" . $res->getAttribute('src');
          break;
        default:
          continue;
      }
    }
      
    file_unmanaged_save_data($str, $this->getDirectory() . '/' . $this->getName() . '.info');
  }
  
  /**
   * Generate path to theme in filesystem.
   *
   * @return string path
   */
  protected function createDirectoryPath() {
    $relative_path = 'sites/all/themes/' . $this->getName();
    return DRUPAL_ROOT . '/' . $relative_path;
  }
  
  /**
   * Unpack a uploaded archive file.
   *
   * @param string $file
   *   The filename of the archive you wish to extract.
   * @param string $directory
   *   The directory you wish to extract the archive into.
   * @return Archiver
   *   The Archiver object used to extract the archive.
   * @throws Exception on failure.
   */
  protected function extractArchive($file, $directory) {
    $archiver = archiver_get_archiver($file);
    if (!$archiver) {
      throw new Exception(t('Cannot extract %file, not a valid archive.', array ('%file' => $file)));
    }
  
    $archiver->extract($directory);
    
    return $archiver;
  }
}



/**
 * Copies a wepbage from url to directory
 * 
 * Also takes care of resources
 */
class Html2themeWebPageCopier {
  
  /**
   * @var string $url to HTML file.
   */
  protected $url;
  
  /**
   * @var string $url to HTML file.
   */
  protected $dir;
  
  /**
   * @var QueryPath $qp QueryPath object of html
   */
  protected $qp;
  
  public function __construct($url, $dir) {
    $this->url = $url;
    $this->dir = $dir;
    $this->copyWebPage();
  }
  
  /**
   * Copy entire webpages with resources
   *
   */
  protected function copyWebPage() {
    //Create some basic folders
    drupal_mkdir($this->dir . "/images");
    drupal_mkdir($this->dir . "/styles");
    drupal_mkdir($this->dir . "/scripts");
    drupal_mkdir($this->dir . "/div"); //All resources we are unable to fit in the other directories
    
    //Copy HTML
    $this->copyFile($this->url, $this->dir . '/index.html');
    
    //Copy resources
    $this->copyResources();
  }

  /**
   * Copy resources from the html file
   */
  protected function copyResources() {
    //Use QueryPath to select all resources
    $qp = htmlqp($this->dir . '/index.html', '[href], [src]');
    
    //Loop through all
    foreach($qp->get() as $res ){
      
      if($res->tagName == 'a') continue;
      
      $dir = '/div';
      switch ($res->tagName){
        case 'link':
          if($res->getAttribute('rel') == 'stylesheet') $dir = '/styles';
          else if(strpos($res->getAttribute('rel'), 'icon')) $dir = '';
          else $dir = '/div';
          break;
        case 'script':
          $dir = '/scripts';
          break;
        case 'img':
          $dir = '/images';
          break;
        case 'input':
          if($res->getAttribute('type') == 'image'){
            $dir = '/images';
            break;
          }
        default:
          $dir = '/div';
      }
      
      $url_type = ($res->getAttribute('src') == '')? 'href' : 'src';
      
      //Get url of resource
      $url = $res->getAttribute($url_type);
      
      //If url was relative
      if(!valid_url($url, TRUE)){
        //Relative path
        $baseurl = (dirname($this->url) != 'http:') ? dirname($this->url) : $this->url;
        $url = $baseurl . '/' . $url;
      }
      
      //Take copy of resource
      $local_path = $this->copyFile($url, $this->dir . $dir);
      
      //Generate relative path
      $new_url = str_replace($this->dir . '/','',$local_path);
      
      //Set link href to new path
      $res->setAttribute($url_type, $new_url);
    }
    
    //Write to html file
    $qp->writeHTML($this->dir . '/index.html');
  }

  
  /**
   * Copy file from a url to a directory
   *
   * @param $url Url to resource we want to copy
   * @param $dir The directory we want to copy the resource into
   * 
   * @return mixed path to file on success or false on failure
   */
  protected function copyFile($url, $dir) {
    $path = system_retrieve_file($url, $dir);
    if(!$path) {
      drupal_set_message(t('Unable to copy %url to %dir.', array('%url' => $url, '%dir' => $dir)), 'error', false);
      return false;
    }
    return $path;
  }
}
