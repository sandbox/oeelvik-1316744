<?php ?>
<div class="html2theme">
  <h1><?php print t('Editing %title > %file', array('%title' => $theme_title, '%file' => $theme_file)); ?></h1>
  <aside class="toolbar">
    <button id="html2theme-tool-delete" class="negative"><img src="<?php print $module_path; ?>/icons/delete.png"><?php print t('Delete'); ?></button>
    <button id="html2theme-tool-clear" class="negative"><img src="<?php print $module_path; ?>/icons/clear.png"><?php print t('Clear'); ?></button>
    <div id="html2theme-insert-dialog">
      <h3>Insert:</h3>
      <div class="content"></div>
      <button id="html2theme-cansel-insert" class="negative"><img src="<?php print $module_path; ?>/icons/delete.png"><?php print t('Cansel'); ?></button>
    </div>
    <button id="html2theme-tool-insert" class="positive"><img src="<?php print $module_path; ?>/icons/insert.png"><?php print t('Insert'); ?></button>
    <button id="html2theme-tool-use-as" class="negative" disabled="disabled"><img src="<?php print $module_path; ?>/icons/use-as.png"><?php print t('Use as'); ?></button>
    <button id="html2theme-tool-Undo" disabled="disabled"><img src="<?php print $module_path; ?>/icons/undo.png"><?php print t('Undo'); ?></button>
    <button id="html2theme-tool-Redo" disabled="disabled"><img src="<?php print $module_path; ?>/icons/redo.png"><?php print t('Redo'); ?></button>
    <input type="checkbox" checked="checked" id="html2theme-tool-filter-containers"/> <label for="html2theme-tool-filter-containers" title="<?php print t('Selects first parent container element of the element you clicked. Use this option to avoid selecting elements such as: a, img, p, input, button, etc.'); ?>"><?php print t('Select container'); ?></label>
    
    <form enctype="multipart/form-data" action="?q=admin/appearance/html2theme/save" method="post" id="html2theme-edit-form" accept-charset="UTF-8">
      <input type="hidden" name="theme_name" value="<?php print $theme_name; ?>" />
      <input type="hidden" name="theme_file" value="<?php print $theme_file; ?>" />
      <input type="hidden" id="html2theme-actions"  name="actions" />
      <div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="<?php print t('Save'); ?>" class="form-submit"></div>
    </form>
    
  </aside>
  <b><?php print t('Selected element:'); ?></b> <span id="html2theme-selection-display"><?php print t('No element selected.'); ?></span>
  <iframe id="html2theme-editor-window" width="100%" height="600" src="<?php print $theme_path; ?>/static/index.html"></iframe>
  <select id="html2theme-editor-history-browser" multiple="multiple" size="15"></select>
  
</div>
